from django.contrib import admin
from .models import Config_menu, Manufac_menu, Gl_register

# Register your models here.
admin.site.register(Config_menu)
admin.site.register(Manufac_menu)
admin.site.register(Gl_register)