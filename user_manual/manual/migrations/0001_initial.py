# Generated by Django 3.2 on 2021-04-15 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Config_menu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('screen_shot', models.ImageField(upload_to='pics/Config_menu')),
                ('menu_name', models.CharField(max_length=100)),
                ('descp', models.TextField()),
            ],
        ),
    ]
