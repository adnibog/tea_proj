# Generated by Django 3.2 on 2021-04-17 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manual', '0002_config_menu_html_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='config_menu',
            name='content',
            field=models.BooleanField(default=0),
        ),
    ]
