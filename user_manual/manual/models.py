from django.db import models

# Create your models here.

# Configuraton menu DB.
class Config_menu(models.Model):
    screen_shot = models.ImageField(upload_to='pics/Config_menu')
    menu_name = models.CharField(max_length=100)
    descp = models.TextField()
    html_name = models.CharField(max_length=200, null=True)
    content = models.BooleanField(default=0)
    sub_content = models.BooleanField(default=0)
    zoom = models.BooleanField(default=0)
    page = models.CharField(max_length=200)
    page_id = models.IntegerField(default=0)

    def __str__(self):
        return self.menu_name

# Green Leaf Register DB.
class Gl_register(models.Model):
    screen_shot = models.ImageField(upload_to='pics/GL_register')
    menu_name = models.CharField(max_length=100)
    descp = models.TextField()
    html_name = models.CharField(max_length=200, null=True)
    content = models.BooleanField(default=0)
    sub_content = models.BooleanField(default=0)
    zoom = models.BooleanField(default=0)
    page = models.CharField(max_length=200)
    page_id = models.IntegerField(default=0)

    def __str__(self):
        return self.menu_name

# Manufacturing menu DB.
class Manufac_menu(models.Model):
    screen_shot = models.ImageField(upload_to='pics/Manufac_menu')
    menu_name = models.CharField(max_length=100)
    descp = models.TextField()
    html_name = models.CharField(max_length=200, null=True)
    content = models.BooleanField(default=0)
    sub_content = models.BooleanField(default=0)
    zoom = models.BooleanField(default=0)
    page = models.CharField(max_length=200)
    page_id = models.IntegerField(default=0)

    def __str__(self):
        return self.menu_name