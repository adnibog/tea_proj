from django.shortcuts import render
from .models import Config_menu, Manufac_menu, Gl_register

# Create your views here.

# <--- Views for Configuration Menu --->
def index(request):
    return render(request, 'index.html')

def manual(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/manual.html', {'config1': config1})

def config_menu(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/config_menu.html', {'config1': config1})

def insert_incen(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen.html', {'config1': config1})

def insert_with_yes(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_with_yes.html', {'config1': config1})

def insert_incen_quality1(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_quality1.html', {'config1': config1})

def insert_incen_quality2(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_quality2.html', {'config1': config1})

def insert_incen_quantity(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_quantity.html', {'config1': config1})

def insert_incen_quantity1(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_quantity1.html', {'config1': config1})

def insert_incen_quantity2(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_quantity2.html', {'config1': config1})

def insert_incen_transp(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_transp.html', {'config1': config1})

def insert_incen_transp_fix(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_transp_fix.html', {'config1': config1})

def insert_incen_transp_fix1(request):
    config1 = Config_menu.objects.all().order_by('page_id')
    return render(request, 'Config/insert_incen_transp_fix1.html', {'config1': config1})

# <--- Done --->

# <--- For Green Leaf Register Menu --->
gl = Gl_register.objects.all().order_by('page_id')
def gl_register_menu(request):
    return render(request, 'Gl_register/gl_register_menu.html', {'gl': gl})

def gl_register(request):
    return render(request, 'Gl_register/gl_register.html', {'gl': gl})

def gl_register_own(request):
    return render(request, 'Gl_register/gl_register_own.html', {'gl': gl})

def gl_register_incent(request):
    return render(request, 'Gl_register/gl_register_incent.html', {'gl': gl})

def edit_gl(request):
    return render(request, 'Gl_register/edit_gl.html', {'gl': gl})

def final_gl(request):
    return render(request, 'Gl_register/final_gl.html', {'gl': gl})


# <--- For Manufacturing Menu --->
manufac = Manufac_menu.objects.all().order_by('page_id')
def manufac_menu(request):
    return render(request, 'Manufac/manufac_menu.html', {'manufac': manufac})

def dryer_mouth(request):
    return render(request, 'Manufac/dryer_mouth.html', {'manufac': manufac})

def packing_register(request):
    return render(request, 'Manufac/packing_register.html', {'manufac': manufac})

def despatch_register(request):
    return render(request, 'Manufac/despatch_register.html', {'manufac': manufac})