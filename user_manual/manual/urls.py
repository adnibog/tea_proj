from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # <--- Views for Configuration Menu --->
    path('', views.index, name='index'),
    path('manual', views.manual, name='manual'),
    path('config_menu', views.config_menu, name='config_menu'),
    path('insert_incen', views.insert_incen, name='insert_incen'),
    path('insert_with_yes', views.insert_with_yes, name='insert_with_yes'),
    path('insert_incen_quality1', views.insert_incen_quality1, name='insert_incen_quality1'),
    path('insert_incen_quality2', views.insert_incen_quality2, name='insert_incen_quality2'),
    path('insert_incen_quantity', views.insert_incen_quantity, name='insert_incen_quantity'),
    path('insert_incen_quantity1', views.insert_incen_quantity1, name='insert_incen_quantity1'),
    path('insert_incen_quantity2', views.insert_incen_quantity2, name='insert_incen_quantity2'),
    path('insert_incen_transp', views.insert_incen_transp, name='insert_incen_transp'),
    path('insert_incen_transp_fix', views.insert_incen_transp_fix, name='insert_incen_transp_fix'),
    path('insert_incen_transp_fix1', views.insert_incen_transp_fix1, name='insert_incen_transp_fix1'),
    # <--- For Green Leaf Register Menu --->
    path('gl_register_menu', views.gl_register_menu, name='gl_register_menu'),
    path('gl_register', views.gl_register, name='gl_register'),
    path('gl_register_own', views.gl_register_own, name='gl_register_own'),
    path('gl_register_incent', views.gl_register_incent, name='gl_register_incent'),
    path('edit_gl', views.edit_gl, name='edit_gl'),
    path('final_gl', views.final_gl, name='final_gl'),
    # <--- For Manufacturing Menu --->
    path('manufac_menu', views.manufac_menu, name='manufac_menu'),
    path('dryer_mouth', views.dryer_mouth, name='dryer_mouth'),
    path('packing_register', views.packing_register, name='packing_register'),
    path('despatch_register', views.despatch_register, name='despatch_register'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)